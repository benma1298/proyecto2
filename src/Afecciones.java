import java.util.*;
public class Afecciones {
	private String Afeccion;
	private double gravedad;
	private boolean TieneAfec=false;
	Afecciones(){
		
	}
	
	public void generarAfeccion() {
		Random TipoAfeccion = new Random();
		int TAfec = TipoAfeccion.nextInt(10);
		if (TAfec<6) {
			Afeccion = "Obesidad";
		}else {
			Afeccion = "Desnutricion";
		}
		TieneAfec=true;
	}
	
	public double generarGravedad() {
		if (Afeccion.contains("Obesidad")) {
			gravedad = 0.2;
		}else if (Afeccion.contains("Desnutricion")) {
			gravedad = 0.1;
		}
		return gravedad;
	}

	public boolean isTieneAfec() {
		return TieneAfec;
	}
	
}
