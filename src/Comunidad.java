import java.util.*;
public class Comunidad {
	
	private int numCiudadanos;
	
	private int promedioConexionFisica;
	
	private String enfermedad;
	
	private int numInfectados;
	
	private double probabilidadConexionFisica;
	
	private ArrayList <Ciudadano> listaperso = new ArrayList();

	
	Comunidad(){
		
	}

	public void creaComunidad() {
		HashSet HenfeBase = new HashSet();
		HashSet Hafec = new HashSet();		
		
		while (HenfeBase.size()<(int)numCiudadanos/4) {//Se crea lista para la cantidad especificada de personas con enfermedades base y se le asigna un i aleatorio
			Random rand = new Random();
			int Rand = rand.nextInt(numCiudadanos);
			HenfeBase.add(Rand);
		}
		
		while (Hafec.size()<(int)numCiudadanos*0.65) {//Se crea lista para la cantidad especificada de personas con afecciones y se les asigna un numero aleatorio i
			Random rand = new Random();
			int Rand = rand.nextInt(numCiudadanos);
			Hafec.add(Rand);
		}
		
		for (int i = 0; i<numCiudadanos; i++) {
			Ciudadano c = new Ciudadano();
			listaperso.add(c);
			c.setId(i);	
			c.setEdad();
			c.setConexionesFisicasConOtros(promedioConexionFisica);
			Random NCiudadano = new Random();
			int NC = NCiudadano.nextInt();
			
			
			if (HenfeBase.contains(i)) {
				Afecciones Afec = new Afecciones();
				c.setAfecc(Afec);
			}
			
			if (Hafec.contains(i)) {
				EnfermedadBase EnfeBase = new EnfermedadBase();
				c.setEnfeBase(EnfeBase);
			}
		
		}
	}
	
	public void creaInteracciones() {
		ArrayList <HashSet> listaI = new ArrayList();
		
		for (int i=0; i<promedioConexionFisica; i++) {
			
		}
	}
	
	public ArrayList<Ciudadano> getlista() {
		return listaperso;
	}
	
	public int getNumCiudadanos() {
		return numCiudadanos;
	}

	public void setNumCiudadanos(int numCiudadanos) {
		this.numCiudadanos = numCiudadanos;
	}

	public int getPromedioConexionFisica() {
		return promedioConexionFisica;
	}

	public void setPromedioConexionFisica(int promedioConexionFisica) {
		this.promedioConexionFisica = promedioConexionFisica;
	}

	public String getEnfermedad() {
		return enfermedad;
	}

	public void setEnfermedad(String enfermedad) {
		this.enfermedad = enfermedad;
	}

	public int getNumInfectados() {
		return numInfectados;
	}

	public void setNumInfectados(int numInfectados) {
		this.numInfectados = numInfectados;
	}

	public double getProbabilidadConexionFisica() {
		return probabilidadConexionFisica;
	}

	public void setProbabilidadConexionFisica(double d) {
		this.probabilidadConexionFisica = d;
	}
	
}
