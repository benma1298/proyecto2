import java.util.*;
public class Enfermedad {
	
	private double infeccionProbable;
	
	private int promedioPasos;
	
	private boolean enfermo=false;
	
	private int contador;
	
	private boolean valor;
	
	private double GravedadDeEnfermedad;
	
	private String estado;
	
	private double ProbabilidadDeMorir= 0.1;
	
	Enfermedad(){
		
	}
	
	public boolean infeccion() {
		Random probaInfectProba = new  Random();
		int probaIProba = probaInfectProba.nextInt(100)+1;
		if (probaIProba<=infeccionProbable*100) {//si el contacto fisico resulta en un contagio de la enfermedad infeccionsa
			valor = true;
		}else {
			valor =false;
		}
		return valor;
	}
	
	public String calculoDeGravedad(double Gravedad, int edad, int Steps, String TipoVacuna) {
		
		this.GravedadDeEnfermedad = Gravedad;
		
		this.GravedadDeEnfermedad = this.GravedadDeEnfermedad + (edad*0.01);
		
		this.ProbabilidadDeMorir = this.ProbabilidadDeMorir + this.GravedadDeEnfermedad;
		
		Random Posibilidad = new Random();
		int Pos = Posibilidad.nextInt(100)+1;
		
		if (this.GravedadDeEnfermedad*100>Pos) {
			estado="Grave";
		}
		Random ProbaMuerte = new Random();
		int PMuerte = ProbaMuerte.nextInt(100)+1;
		if (Steps==this.promedioPasos && this.GravedadDeEnfermedad*100>Pos) {
			
			if (TipoVacuna.contains("Vacuna1")) {
				this.ProbabilidadDeMorir = this.ProbabilidadDeMorir/4;
			}
			else if (TipoVacuna.contains("Vacuna2")) {
				this.ProbabilidadDeMorir = 0.0;
			}
			else if (TipoVacuna.contains("Vacuna3")) {
				this.ProbabilidadDeMorir = 0.0;
			}
			
			if (this.ProbabilidadDeMorir*100>PMuerte) {
				estado="Muerto";
			}
			else {
				estado="Sano";
			}
		}else {
			if (this.ProbabilidadDeMorir*100>PMuerte) {
				estado="Muerto";
			}
		}
		
		return estado;
	}

	public double getInfeccionProbable() {
		return infeccionProbable;
	}

	public void setInfeccionProbable(double d) {
		this.infeccionProbable = d;
	}

	public int getPromedioPasos() {
		return promedioPasos;
	}

	public void setPromedioPasos(int promedioPasos) {
		this.promedioPasos = promedioPasos;
	}

	public boolean getEnfermo() {
		return enfermo;
	}

	public void setEnfermo(boolean enfermo) {
		this.enfermo = enfermo;
	}

	public int getContador() {
		return contador;
	}

	public void setContador(int contador) {
		this.contador = contador;
	}
}
