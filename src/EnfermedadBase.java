import java.util.*;
public class EnfermedadBase {
	private String EnfermedadB;
	private double gravedad;
	private boolean TieneEnfeB=false;
	EnfermedadBase(){
		
	}
	
	public void generarEnfermedadBase() {
		Random EnfeBase = new Random();
		int EnfeB = EnfeBase.nextInt(10)+1;
		if (EnfeB<3) {
			EnfermedadB = "Enfermedad Cerebrovascular";
		}else if (EnfeB<6 && EnfeB>3) {
			EnfermedadB = "Asma";
		}else if (EnfeB<9 && EnfeB>6) {
			EnfermedadB = "Hipertencion";
		}else if (EnfeB>9) {
			EnfermedadB = "Cancer";
		}
		TieneEnfeB=true;
	}
	
	public double generarGravedad() {
		if (EnfermedadB.contains("Enfermedad Cerebrovascular")) {
			gravedad = 0.6;
		}else if (EnfermedadB.contains("Asma")) {
			gravedad = 0.3;
		}else if (EnfermedadB.contains("Hipertencion")) {
			gravedad = 0.5;
		}else if (EnfermedadB.contains("Cancer")) {
			gravedad = 0.8;
		}
		return gravedad;
	}

	public boolean isTieneEnfeB() {
		return TieneEnfeB;
	}
	
}
