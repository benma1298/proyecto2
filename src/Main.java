
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Enfermedad ml = new Enfermedad();
		ml.setInfeccionProbable(0.3);
		ml.setPromedioPasos(18);
		
		Comunidad com = new Comunidad();
		com.setNumCiudadanos(2000);
		com.setPromedioConexionFisica(8);
		com.setEnfermedad("malaria");
		com.setNumInfectados(10);
		com.setProbabilidadConexionFisica(0.9);
		com.creaComunidad();
		
		Simulador s = new Simulador();
		s.setPasos(45);
		s.añadirComunidad(com);
		s.añadirEnfermedad(ml);
		s.simulacionDePasos();
		
	}

}
